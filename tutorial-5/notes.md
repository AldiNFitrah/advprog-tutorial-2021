# Tutorial-5 Requirements

## Already Done

1. Mahasiswa Service:
    - Model:
        ```
        npm     String [Primary Key]
        nama    String
        email   String
        ipk     String
        noTelp  String
        ```

    - Service, with query defined:
        - createMahasiswa
        - getListMahasiswa
        - getMahasiswaByNPM
        - updateMahasiswa
        - deleteMahasiswaByNPM
    
    - Standard CRUD Endpoints with base `/mahasiswa`

1. MataKuliah Service:
    - Model:
        ```
        kode_matkul String [Primary Key]
        nama_matkul String
        prodi       String
        ```

    - Service, with query defined:
        - createMataKuliah
        - getListMataKuliah
        - getMataKuliah (by kodeMatkul)
        - updateMataKuliah
        - deleteMataKuliah (by kodeMatkul)
    
    - Standard CRUD Endpoints with base `/mata-kuliah`


## ToDo

1. Create a OneToMany relationship between MataKuliah and Mahasiswa, with the following specifications:
    - Every Mahasiswa that applied to a MataKuliah will be accepted without any selection
    - MataKuliah that already has some Mahasiswa can't be deleted
    - Mahasiswa who are listed in a MataKuliah can't be deleted

2. Create Log Model with the following specifications:
    - Fields:
        ```
        id          integer [Primary Key, Auto Increment]
        start_time  datetime
        end_time    datetime
        description Text
        ```
    - ManyToOne related to Mahasiswa
    - Simple CRUD operations must be provided
    - Provide one endpoint that returns the log summary of a Mahasiswa for the requested months.
        - To simplify this one, we will ignore the year constraint
        - Returned value example:
            ```json
            [
                {
                    "month": "January",
                    "workHours": 10,
                    "salary": 3500
                },
                {
                    "month": "February",
                    "workHours": 40,
                    "salary": 14000
                }
            ]
            ```
    - The salary of Mahasiswa is 350 Greil/hour
