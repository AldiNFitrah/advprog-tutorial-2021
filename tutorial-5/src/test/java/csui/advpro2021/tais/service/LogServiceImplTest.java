package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static csui.advpro2021.tais.misc.Utils.getLocalDateTimeFromString;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906285472");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        LocalDateTime startTime = getLocalDateTimeFromString("2021-04-03 08:00");
        LocalDateTime endTime = getLocalDateTimeFromString("2021-04-03 10:00");
        log = new Log(startTime, endTime, "ngajar");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreateLog(){
        lenient().when(logService.createLog(log)).thenReturn(log);
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        assertEquals(log, logService.createLog(log, mahasiswa.getNpm()));
    }

    @Test
    public void testServiceCreateLogMahasiswaNotFound(){
        lenient().when(logService.createLog(log)).thenReturn(log);
        when(mahasiswaRepository.findByNpm(any())).thenReturn(null);

        assertNull(logService.createLog(log, mahasiswa.getNpm()));
    }

    @Test
    public void testServiceGetListLog(){
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogResult = logService.getListLog();
        Assertions.assertIterableEquals(listLog, listLogResult);
    }

    @Test
    public void testServiceGetLogById(){
        lenient().when(logService.getLogById(log.getId())).thenReturn(log);
        Log resultLog = logService.getLogById(log.getId());
        assertEquals(log.getId(), resultLog.getId());
    }

    @Test
    public void testServiceDeleteLog(){
        logService.createLog(log);
        logService.deleteLogById(log.getId());
        lenient().when(logService.getLogById(log.getId())).thenReturn(log);
        assertEquals(log, logService.getLogById(log.getId()));
    }

    @Test
    public void testServiceUpdateLog(){
        logService.createLog(log);
        String currentDescription = log.getDescription();
        //Change description from ngajar to ngawas
        log.setDescription("ngawas");

        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        Log resultLog = logService.updateLog(log.getId(), log, log.getMahasiswa().getNpm());

        assertNotEquals(resultLog.getDescription(), currentDescription);
        assertEquals(resultLog.getStartTime(), log.getStartTime());
    }

    @Test
    public void testServiceUpdateLogMahasiswaNotFound() {
        logService.createLog(log);
        String currentDescription = log.getDescription();
        //Change description from ngajar to ngawas
        log.setDescription("ngawas");

        when(mahasiswaRepository.findByNpm(any())).thenReturn(null);

        assertNull(logService.updateLog(log.getId(), log, "1906"));
    }

    @Test
    public void testGetLogByMahasiswa() {
        logService.createLog(log);

        when(logRepository.findByMahasiswaOrderByStartTimeAsc(mahasiswa)).thenReturn(Lists.list(log));

        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        assertEquals(Lists.list(log), logService.getLogByMahasiswa(mahasiswa.getNpm()));
    }

}
