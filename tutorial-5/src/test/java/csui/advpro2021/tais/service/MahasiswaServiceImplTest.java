package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static csui.advpro2021.tais.misc.Utils.getLocalDateTimeFromString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class MahasiswaServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private Log log;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        LocalDateTime startTime = getLocalDateTimeFromString("2021-04-03 08:00");
        LocalDateTime endTime = getLocalDateTimeFromString("2021-04-03 10:00");
        log = new Log(startTime, endTime, "ngajar");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreateMahasiswa(){
        lenient().when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
    }

    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }

    @Test
    public void testServiceDeleteMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM("1906192052");
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(null);
        assertEquals(null, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test
    public void testServiceUpdateMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        String currentIpkValue = mahasiswa.getIpk();
        //Change IPK from 4 to 3
        mahasiswa.setIpk("3");

        lenient().when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa)).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);

        assertNotEquals(resultMahasiswa.getIpk(), currentIpkValue);
        assertEquals(resultMahasiswa.getNama(), mahasiswa.getNama());
    }

    @Test
    void testGetLogSummary() {
        LocalDateTime startTime = getLocalDateTimeFromString("2021-05-03 08:00");
        LocalDateTime endTime = getLocalDateTimeFromString("2021-05-03 10:00");
        Log log2 = new Log(startTime, endTime, "ngajar lagi");
        log2.setMahasiswa(mahasiswa);

        mahasiswaService.createMahasiswa(mahasiswa);
        logService.createLog(log);
        logService.createLog(log2);

        lenient().when(logRepository.findByMahasiswaOrderByStartTimeAsc(any())).thenReturn(Lists.newArrayList(log, log2));

        List<Map<String, Object>> result = Lists.newArrayList(mahasiswaService.getLogSummary(mahasiswa.getNpm(), "4", null));
        Assertions.assertEquals(1, result.size());
        Assertions.assertTrue(result.get(0).containsKey("salary"));
        Assertions.assertEquals(700.0, result.get(0).get("salary"));
    }

}
