package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MataKuliahServiceImplTest {
    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private MataKuliahServiceImpl mataKuliahService;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private MataKuliah matkul;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah();
        matkul.setKodeMatkul("ADVPROG");
        matkul.setNama("Advanced Programming");
        matkul.setProdi("Ilmu Komputer");

        mahasiswa = new Mahasiswa("1906285472", "Aldi", null, null, null);
    }

    @Test
    void testServiceGetListMataKuliah() {
        Iterable<MataKuliah> listMataKuliah = mataKuliahRepository.findAll();
        lenient().when(mataKuliahService.getListMataKuliah()).thenReturn(listMataKuliah);
        Iterable<MataKuliah> listMataKuliahResult = mataKuliahService.getListMataKuliah();
        Assertions.assertIterableEquals(listMataKuliah, listMataKuliahResult);
    }

    @Test
    void testServiceGetMataKuliah() {
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        MataKuliah resultMatkul = mataKuliahService.getMataKuliah(matkul.getKodeMatkul());
        Assertions.assertEquals(matkul.getKodeMatkul(), resultMatkul.getKodeMatkul());
    }

    @Test
    void testServiceCreateMataKuliah() {
        lenient().when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);
        MataKuliah resultMatkul = mataKuliahService.createMataKuliah(matkul);
        Assertions.assertEquals(matkul.getKodeMatkul(), resultMatkul.getKodeMatkul());
    }

    @Test
    void testServiceUpdateMataKuliah() {
        mataKuliahService.createMataKuliah(matkul);
        String namaMatkul = "ADV125YIHA";
        matkul.setNama(namaMatkul);
        MataKuliah expectedMatkul = matkul;
        expectedMatkul.setNama(namaMatkul);
        MataKuliah resultMatkul = mataKuliahService.updateMataKuliah(matkul.getKodeMatkul(), matkul);
        Assertions.assertEquals(expectedMatkul.getNama(), resultMatkul.getNama());
    }

    @Test
    void testServiceDeleteMataKuliah() {
        mataKuliahService.createMataKuliah(matkul);
        mataKuliahService.deleteMataKuliah(matkul.getKodeMatkul());
        Assertions.assertEquals(null, mataKuliahService.getMataKuliah(matkul.getKodeMatkul()));
    }

    @Test
    void testAddMahasiswaSuccess() {
        mataKuliahService.createMataKuliah(matkul);
        mahasiswaService.createMahasiswa(mahasiswa);

        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        Assertions.assertEquals("202", mataKuliahService.addMahasiswa(matkul.getKodeMatkul(), mahasiswa.getNpm()));
    }

    @Test
    void testAddMahasiswaNotFound() {
        mataKuliahService.createMataKuliah(matkul);

        Assertions.assertEquals("404", mataKuliahService.addMahasiswa(matkul.getKodeMatkul(), mahasiswa.getNpm()));
    }

    @Test
    void testAddMahasiswaConflict() {
        mataKuliahService.createMataKuliah(matkul);

        MataKuliah otherMatkul = new MataKuliah("OS", "Operating System", "Ilkom");
        mataKuliahService.createMataKuliah(otherMatkul);

        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mahasiswa.setMataKuliah(otherMatkul);
        mahasiswaService.createMahasiswa(mahasiswa);

        Assertions.assertEquals("409", mataKuliahService.addMahasiswa(matkul.getKodeMatkul(), mahasiswa.getNpm()));
    }

}
