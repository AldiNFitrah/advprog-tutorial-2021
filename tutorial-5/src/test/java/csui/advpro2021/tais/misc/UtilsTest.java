package csui.advpro2021.tais.misc;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Map;

import static csui.advpro2021.tais.misc.Utils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UtilsTest {
    @Test
    void testGetValueOrDefault() throws Exception {
        assertEquals(10, getValueOrDefault(null, 10));
    }

    @Test
    void testGetLocalDateFromString() {
        LocalDateTime result = getLocalDateTimeFromString("2021-04-04 08:00");

        assertEquals(4, result.getDayOfMonth());
        assertEquals(4, result.getMonthValue());
        assertEquals(2021, result.getYear());
    }

    @Test
    void testGetMonthlyLogSummary() {
        Map<String, Object> result = getMonthlyLogSummary(4, 2, 350);

        assertEquals(700.0, (double) result.get("salary"));
    }

}
