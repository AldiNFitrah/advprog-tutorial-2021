package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = MataKuliahController.class)
class MataKuliahControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    private MataKuliah matkul;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
        mahasiswa = new Mahasiswa("1906285472", "Aldi", null, null, null);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetListMataKuliah() throws Exception {
        Iterable<MataKuliah> listMataKuliah = Arrays.asList(matkul);
        when(mataKuliahService.getListMataKuliah()).thenReturn(listMataKuliah);

        mvc.perform(get("/mata-kuliah").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].kodeMatkul").value(matkul.getKodeMatkul()));
    }

    @Test
    void testControllerCreateMataKuliah() throws Exception {
        when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);

        mvc.perform(post("/mata-kuliah")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(matkul)))
                .andExpect(jsonPath("$.kodeMatkul").value(matkul.getKodeMatkul()));
    }

    @Test
    void testControllerGetMataKuliah() throws Exception {
        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        mvc.perform(get("/mata-kuliah/" + matkul.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value(matkul.getNama()));
    }

    @Test
    public void testControllerGetNonExistMataKuliah() throws Exception{
        mvc.perform(get("/mata-kuliah/BASDEAD").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerUpdateMataKuliah() throws Exception {
        mataKuliahService.createMataKuliah(matkul);

        String namaMatkul = "ADV125YIHA";
        matkul.setNama(namaMatkul);

        when(mataKuliahService.updateMataKuliah(matkul.getKodeMatkul(), matkul)).thenReturn(matkul);

        mvc.perform(put("/mata-kuliah/" + matkul.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(matkul)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value(namaMatkul));
    }

    @Test
    void testControllerDeleteMataKuliah() throws Exception {
        mataKuliahService.createMataKuliah(matkul);
        mvc.perform(delete("/mata-kuliah/" + matkul.getKodeMatkul()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void testAddMahasiswaSuccess() throws Exception {
        mataKuliahService.createMataKuliah(matkul);
        mahasiswaService.createMahasiswa(mahasiswa);

        Map<String, Object> body = new HashMap<>();
        body.put("npm", mahasiswa.getNpm());

        when(mataKuliahService.addMahasiswa(matkul.getKodeMatkul(), mahasiswa.getNpm())).thenReturn("202");

        mvc.perform(post("/mata-kuliah/" + matkul.getKodeMatkul() + "/mahasiswa")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(body)))
                .andExpect(status().isAccepted());
    }

    @Test
    void testAddMahasiswaNotFound() throws Exception {
        mataKuliahService.createMataKuliah(matkul);
        mahasiswaService.createMahasiswa(mahasiswa);

        when(mataKuliahService.addMahasiswa("abc", mahasiswa.getNpm())).thenReturn("404");
        when(mahasiswaService.getMahasiswaByNPM(eq(mahasiswa.getNpm()))).thenReturn(mahasiswa);

        mvc.perform(post("/mata-kuliah/" + "abc" + "/mahasiswa")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"npm\": \"1906285472\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testAddMahasiswaConflict() throws Exception {
        MataKuliah otherMatkul = new MataKuliah("OS", "Operating System", "Ilmu Komputer");

        mataKuliahService.createMataKuliah(matkul);
        mataKuliahService.createMataKuliah(otherMatkul);
        mahasiswaService.createMahasiswa(mahasiswa);

        Map<String, Object> body = new HashMap<>();
        body.put("npm", mahasiswa.getNpm());

        when(mataKuliahService.addMahasiswa(matkul.getKodeMatkul(), mahasiswa.getNpm())).thenReturn("202");
        when(mataKuliahService.addMahasiswa(otherMatkul.getKodeMatkul(), mahasiswa.getNpm())).thenReturn("409");

        mvc.perform(post("/mata-kuliah/" + otherMatkul.getKodeMatkul() + "/mahasiswa")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(body)))
                .andExpect(status().is4xxClientError());
    }

}
