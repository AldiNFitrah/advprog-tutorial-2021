package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;

import static csui.advpro2021.tais.misc.Utils.localDateTimeFormatter;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;
    private Log log;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        log = new Log(LocalDateTime.parse("2021-04-01 07:00", localDateTimeFormatter), LocalDateTime.parse("2021-04-01 13:00", localDateTimeFormatter), "makan");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testControllerPostLog() throws Exception {

        when(logService.createLog(any(), eq(mahasiswa.getNpm()))).thenReturn(log);

        mvc.perform(post("/log")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content("{\n" +
                        "    \"startTime\": \"2021-08-01 04:00\",\n" +
                        "    \"endTime\": \"2021-08-01 08:00\",\n" +
                        "    \"description\": \"makan\",\n" +
                        "    \"npm\": \"1906192052\"\n" +
                        "}"))
                .andExpect(jsonPath("$.description").value("makan"));
    }

    @Test
    public void testControllerGetListLog() throws Exception {

        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/log").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].description").value("makan"));
    }

    @Test
    public void testControllerGetLogById() throws Exception {

        when(logService.getLogById(log.getId())).thenReturn(log);
        mvc.perform(get("/log/" + log.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("makan"));
    }

    @Test
    public void testControllerGetNonExistLog() throws Exception {
        mvc.perform(get("/log/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdateLog() throws Exception {

        logService.createLog(log);

        //Update log object nama
        log.setDescription("mandi");
        System.out.println(logService.getListLog());

        when(logService.updateLog(eq(log.getId()), any(), any())).thenReturn(log);

        mvc.perform(put("/log/" + log.getId()).contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"startTime\":\"2021-04-03 00:00\",\n" +
                        "    \"endTime\":\"2021-04-03 03:00\",\n" +
                        "    \"description\": \"mandi\",\n" +
                        "    \"npm\": \"1906192052\"\n" +
                        "}"))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value("mandi"));
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        logService.createLog(log);
        mvc.perform(delete("/log/" + log.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}
