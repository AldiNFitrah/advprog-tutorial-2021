package csui.advpro2021.tais.misc;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Utils {

    public static final DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public static LocalDateTime getLocalDateTimeFromString(String dateTimeStr) {
        return LocalDateTime.parse(dateTimeStr, localDateTimeFormatter);
    }

    public static <T> T getValueOrDefault(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }

    public static Map<String, Object> getMonthlyLogSummary(int month, double hours, int salary) {
        Map<String, Object> result = new HashMap<>();
        result.put("month", Month.of(month).getDisplayName(TextStyle.FULL, Locale.ENGLISH));
        result.put("workHours", hours);
        result.put("salary", hours * salary);

        return result;
    }

}
