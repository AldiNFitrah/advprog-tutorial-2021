package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;

public interface LogService {
    Log createLog(Log log);

    Log createLog(Log log, String npm);

    Iterable<Log> getListLog();

    Log getLogById(int id);

    Log updateLog(int id, Log log, String npm);

    void deleteLogById(int id);
}
