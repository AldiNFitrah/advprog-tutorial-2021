package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(Log log) {
        logRepository.save(log);
        return log;
    }
    @Override
    public Log createLog(Log log, String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null) {
            return null;
        }

        log.setMahasiswa(mahasiswa);
        return this.createLog(log);
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log getLogById(int id) {
        return logRepository.findById(id);
    }

    @Override
    public Log updateLog(int id, Log log, String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        if (mahasiswa == null) {
            return null;
        }

        log.setId(id);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogById(int id) {
        logRepository.deleteById(id);
    }

    public List<Log> getLogByMahasiswa(String npm) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        return logRepository.findByMahasiswaOrderByStartTimeAsc(mahasiswa);
    }
}
