package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MataKuliahServiceImpl implements MataKuliahService {
    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Iterable<MataKuliah> getListMataKuliah() {
        return mataKuliahRepository.findAll();
    }

    @Override
    public MataKuliah getMataKuliah(String kodeMatkul) {
        return mataKuliahRepository.findByKodeMatkul(kodeMatkul);
    }

    @Override
    public MataKuliah createMataKuliah(MataKuliah mataKuliah) {
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public MataKuliah updateMataKuliah(String kodeMatkul, MataKuliah mataKuliah) {
        mataKuliah.setKodeMatkul(kodeMatkul);
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public void deleteMataKuliah(String kodeMatkul) {
        MataKuliah matkul = this.getMataKuliah(kodeMatkul);
        mataKuliahRepository.delete(matkul);
    }

    @Override
    public String addMahasiswa(String kodeMatkul, String npm) {
        MataKuliah matkul = this.getMataKuliah(kodeMatkul);
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);

        if (matkul == null || mahasiswa == null) {
            return "404";
        }

        if (mahasiswa.getMataKuliah() != null) {
            return "409";
        }

        mahasiswa.setMataKuliah(matkul);
        mahasiswaRepository.save(mahasiswa);

        return "202";
    }
}
