package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static csui.advpro2021.tais.misc.Utils.getMonthlyLogSummary;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private LogRepository logRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        mahasiswa.setNpm(npm);
        mahasiswaRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        mahasiswaRepository.deleteById(npm);
    }

    public Iterable<Map<String, Object>> getLogSummary(String npm, String startMonth, String endMonth) {
        if (endMonth == null) {
            return getLogSummary(npm, startMonth, startMonth);
        }

        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        List<Log> logList = logRepository.findByMahasiswaOrderByStartTimeAsc(mahasiswa);

        int startMonthInt = Math.max(Integer.parseInt(startMonth), 1);
        int endMonthInt = Math.max(startMonthInt, Math.min(Integer.parseInt(endMonth), 12));

        double[] minutesInMonth = new double[15];
        for (Log log: logList) {
            int thisMonth = log.getStartTime().getMonthValue();
            if (thisMonth < startMonthInt || thisMonth > endMonthInt) {
                continue;
            }

            long minutesDiff = ChronoUnit.MINUTES.between(log.getStartTime(), log.getEndTime());
            minutesInMonth[thisMonth] += minutesDiff;
        }

            List<Map<String, Object>> result = new ArrayList<>();
        for (int i = startMonthInt; i <= endMonthInt; i++) {
            result.add(getMonthlyLogSummary(i, minutesInMonth[i] / 60.0, Log.SALARY_PER_HOUR));
        }

        return result;
    }
}
