package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Map;

import static csui.advpro2021.tais.misc.Utils.getLocalDateTimeFromString;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@RequestBody Map<String, String> requestBody) {
        LocalDateTime startTime = getLocalDateTimeFromString(requestBody.get("startTime"));
        LocalDateTime endTime = getLocalDateTimeFromString(requestBody.get("endTime"));

        Log log = new Log(startTime, endTime, requestBody.get("description"));
        return new ResponseEntity(logService.createLog(log, requestBody.get("npm")), HttpStatus.CREATED);
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "id") int id) {
        Log log = logService.getLogById(id);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "id") int id, @RequestBody Map<String, String> requestBody) {

        LocalDateTime startTime = getLocalDateTimeFromString(requestBody.get("startTime"));
        LocalDateTime endTime = getLocalDateTimeFromString(requestBody.get("endTime"));

        Log log = new Log(startTime, endTime, requestBody.get("description"));
        return new ResponseEntity(logService.updateLog(id, log, requestBody.get("npm")), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "id") int id) {
        logService.deleteLogById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
