package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "Dor! Dor! Dor!";
    }

    @Override
    public String getType() {
        return "Gun";
    }
}
