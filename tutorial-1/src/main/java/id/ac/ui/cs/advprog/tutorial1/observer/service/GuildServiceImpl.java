package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {

    private final QuestRepository questRepository;
    private final Guild guild;
    // private final Adventurer agileAdventurer;
    // private final Adventurer knightAdventurer;
    // private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();

        guild.add(new AgileAdventurer(this.guild));
        guild.add(new KnightAdventurer(this.guild));
        guild.add(new MysticAdventurer(this.guild));
    }

    @Override
    public List<Adventurer> getAdventurers() {
        return guild.getAdventurers();
    }

    @Override
    public void addQuest(Quest quest) {
        this.questRepository.save(quest);
        guild.addQuest(quest);
    }
}
