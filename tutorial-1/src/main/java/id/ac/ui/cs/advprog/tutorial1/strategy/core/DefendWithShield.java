package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    @Override
    public String defend() {
        return "S.H.I.E.L.D.";
    }

    @Override
    public String getType() {
        return "Shield";
    }
}
